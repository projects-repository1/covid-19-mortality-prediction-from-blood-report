from flask import Flask, render_template, request
import pickle

with open("./model.pkl", "rb") as file:
    model = pickle.load(file)

app = Flask(__name__)


@app.route("/", methods=['GET'])
def page1():
    return render_template("body.html")


@app.route("/prediction", methods=['GET'])
def page2():
    age = float(request.args.get("age"))
    eosinophilia = float(request.args.get("eosinophilia"))
    platelet_count = float(request.args.get("platelet_count"))
    prothrombin_activity = float(request.args.get("prothrombin_activity"))
    neutrophils_count = float(request.args.get("neutrophils_count"))
    lymphocytes = float(request.args.get("lymphocytes"))
    d_dimer = float(request.args.get("d_dimer"))
    lactate_dehydrogenase = float(request.args.get("lactate_dehydrogenase"))
    ncov_nucleic_acid_detection = float(request.args.get("nCoV_nucleic_acid_detection"))
    hypersensitive_c_reactive_protein = float(request.args.get("hypersensitive_c_reactive_protein"))
    thrombocytocrit = float(request.args.get("thrombocytocrit"))

    output = model.predict([[age, eosinophilia, platelet_count, prothrombin_activity, neutrophils_count, lymphocytes,
                             d_dimer, lactate_dehydrogenase, ncov_nucleic_acid_detection,
                             hypersensitive_c_reactive_protein, thrombocytocrit]])
    data = [age, eosinophilia, platelet_count, prothrombin_activity, neutrophils_count, lymphocytes, d_dimer,
            lactate_dehydrogenase, ncov_nucleic_acid_detection, hypersensitive_c_reactive_protein, thrombocytocrit]

    if output[0] == 0:
        return render_template('output0.html', data=data)
    if output[0] == 1:
        return render_template('output1.html', data=data)


app.run(port=4000, host="localhost", debug=True)
